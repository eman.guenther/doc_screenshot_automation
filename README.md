# Documentation automation with screenshots

This collection of scripts and files can be used to automate the build
of simple documentation files.
Therefor it uses `pandoc` for converting the documentation from
Markdown to PDF.

## Dependencies

* pandoc >= 2.5
* pdfTeX (TeX Live)
* ImageMagick >= 6.9.103

## Usage

The documentation can be written using the `doc_example.md`.
The necessary meta data entries are already given there, but some of
the values have to be replaced in order to personalize the
documentation.
Also some parameters have to be replaced in `mv-crop.sh`.

After that, run `mv-crop.sh` to move all screenshots in the working
directory and crop them (removing the display manager's panel).

Finally run `make-doc.sh` for creating the documentation as PDF file
using the specified documentation file (has to be written in Markdown).
