#!/bin/sh

mv <screenshot-dir> <dest-dir>

if [ $? -eq 0 ]; then
    for file in ./screenshots/*; do
        convert $file -crop <dimensions> $file
        echo "converted $file"
    done
fi

