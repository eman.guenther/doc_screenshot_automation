#!/bin/sh

# https://pandoc.org/MANUAL.html -> Variables for LaTeX

if [ $# -ne 2 ]; then
    echo "usage: $0 <input-md> <output-pdf>"
    exit 1;
else
    pandoc -s -f markdown -t latex $1 -o $2
    echo "$2 created"
fi

